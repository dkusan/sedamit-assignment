# Application
1. Run the application
```
$ cd sedamit-assignment
$ ./gradlew clean build && java -jar build/libs/assignment-0.0.1-SNAPSHOT.jar
```
_Application will become available on localhost:8080_
## Endpoints
1. Get all people / Retrieve initial data set
Request:
```
$ curl --location --request GET 'localhost:8080/people'
```
Response:
```
[{"id":1,"name":"Clarice","surname":"Priest","telephone":"631-255-1477","cellphone":"202-555-0167","mail":"ClariceCPriest@armyspy.com","adress":"1056 Clark Street","city":"New York"},{"id":2,"name":"Sheryl","surname":"Wright","telephone":"610-271-7835","cellphone":"202-555-0171","mail":"SherylCWright@dayrep.com","adress":"2397 Tree Top Lane","city":"Cleveland"},{"id":3,"name":"Mike","surname":"Biggs","telephone":"336-456-0878","cellphone":"202-555-0106","mail":"MichaelSBiggs@teleworm.us","adress":"4792 Keyser Ridge Road","city":"San Francisco"}]
```
2. Get person by id
Request:
```
curl --location --request GET 'localhost:8080/people/1'
```
Response:
```
{"id":1,"name":"Clarice","surname":"Priest","telephone":"631-255-1477","cellphone":"202-555-0167","mail":"ClariceCPriest@armyspy.com","adress":"1056 Clark Street","city":"New York"}
```
3. Create new person
Request:
```
curl --location --request POST 'localhost:8080/people' \
--header 'Content-Type: application/json' \
--data-raw '{
                "name": "Mike",
                "surname": "Biggs",
                "telephone": "336-456-0878",
                "cellphone": "202-555-0106",
                "mail": "MichaelSBiggs@teleworm.us",
                "adress": "4792 Keyser Ridge Road",
                "city": "Cleveland"
            }'
```
Response:
```
{"id":4,"name":"Mike","surname":"Biggs","telephone":"336-456-0878","cellphone":"202-555-0106","mail":"MichaelSBiggs@teleworm.us","adress":"4792 Keyser Ridge Road","city":"Cleveland"}
```
4. Update person
Request:
```
curl --location --request PUT 'localhost:8080/people/2' \
--header 'Content-Type: application/json' \
--data-raw '{ "name": "Clarice" }'
```
Response:
```
{"id":2,"name":"Clarice","surname":"Wright","telephone":"610-271-7835","cellphone":"202-555-0171","mail":"SherylCWright@dayrep.com","adress":"2397 Tree Top Lane","city":"Cleveland"}
```
5. Delete person by id
Request:
```
curl --location --request DELETE 'localhost:8080/people/3'
```
Response:
```
204 No Content
```
6. Search people by name and/or surname
6.1. Request by name:
Request:
```
curl --location --request GET 'localhost:8080/search?name=Clarice'
```
Response:
```
[{"id":1,"name":"Clarice","surname":"Priest","telephone":"631-255-1477","cellphone":"202-555-0167","mail":"ClariceCPriest@armyspy.com","adress":"1056 Clark Street","city":"New York"},{"id":2,"name":"Clarice","surname":"Wright","telephone":"610-271-7835","cellphone":"202-555-0171","mail":"SherylCWright@dayrep.com","adress":"2397 Tree Top Lane","city":"Cleveland"}]
```
6.2. Request by surname
Request:
```
curl --location --request GET 'localhost:8080/search?surname=Wright'
```
Response:
```
[{"id":2,"name":"Clarice","surname":"Wright","telephone":"610-271-7835","cellphone":"202-555-0171","mail":"SherylCWright@dayrep.com","adress":"2397 Tree Top Lane","city":"Cleveland"}]
```
6.3. Request by name and surname
Request:
```
curl --location --request GET 'localhost:8080/search?name=Mike&surname=Biggs'
```
Response:
```
[{"id":3,"name":"Mike","surname":"Biggs","telephone":"336-456-0878","cellphone":"202-555-0106","mail":"MichaelSBiggs@teleworm.us","adress":"4792 Keyser Ridge Road","city":"San Francisco"},{"id":4,"name":"Mike","surname":"Biggs","telephone":"336-456-0878","cellphone":"202-555-0106","mail":"MichaelSBiggs@teleworm.us","adress":"4792 Keyser Ridge Road","city":null}]
```

7. Search people by City or Address
7.1 Request by city
Request:
```
curl --location --request GET 'localhost:8080/people/location/New%20York'
```
Response:
```
[{"id":1,"name":"Clarice","surname":"Priest","telephone":"631-255-1477","cellphone":"202-555-0167","mail":"ClariceCPriest@armyspy.com","adress":"1056 Clark Street","city":"New York"}]
```
7.2 Request by address
Request:
```
curl --location --request GET 'localhost:8080/people/location/2397%20Tree%20Top%20Lane'
```
Response:
```
[{"id":2,"name":"Sheryl","surname":"Wright","telephone":"610-271-7835","cellphone":"202-555-0171","mail":"SherylCWright@dayrep.com","adress":"2397 Tree Top Lane","city":"Cleveland"}]
```