package com.sedamit.assignment;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@Controller
public class PeopleController {

    private final PeopleService peopleService = new PeopleService();

    @GetMapping(value = "/people/{id}")
    public @ResponseBody Person getPerson(@PathVariable(name = "id") Integer id) {
        return this.peopleService.getPerson(id);
    }

    @GetMapping(value = "/people")
    public @ResponseBody ArrayList<Person> getPeople() {
        return this.peopleService.getPeople();
    }

    @PostMapping(value = "/people")
    public @ResponseBody Person createPerson(@RequestBody Person jsonString) {
        return this.peopleService.createPerson(jsonString);
    }

    @PutMapping(value = "/people/{id}")
    public @ResponseBody Person updatePerson(@PathVariable(name = "id") Integer id, @RequestBody Person jsonString) {
        return this.peopleService.updatePerson(id, jsonString);
    }

    @DeleteMapping(value = "/people/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deletePerson(@PathVariable(name = "id") Integer id) {
        this.peopleService.deletePerson(id);
    }

    @GetMapping(value = "/search")
    public @ResponseBody ArrayList<Person> searchPeople(
            @RequestParam(required = false, name = "name") String name,
            @RequestParam(required = false, name = "surname") String surname
    ) {
        return this.peopleService.searchPeople(name, surname);
    }

    @GetMapping(value = "/people/location/{location}")
    public @ResponseBody ArrayList<Person> searchPeopleByLocation(@PathVariable(name = "location") String location
    ) {
        return this.peopleService.searchPeopleByLocation(location);
    }
}
