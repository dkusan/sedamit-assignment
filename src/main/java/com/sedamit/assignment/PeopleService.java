package com.sedamit.assignment;

import java.util.ArrayList;

public class PeopleService {

    private ArrayList<Person> people;

    public PeopleService() {

        Person person1 = new Person(
                1,
                "Clarice",
                "Priest",
                "631-255-1477",
                "202-555-0167",
                "ClariceCPriest@armyspy.com",
                "1056 Clark Street",
                "New York"
        );

        Person person2 = new Person(
                2,
                "Sheryl",
                "Wright",
                "610-271-7835",
                "202-555-0171",
                "SherylCWright@dayrep.com",
                "2397 Tree Top Lane",
                "Cleveland"
        );

        Person person3 = new Person(
                3,
                "Mike",
                "Biggs",
                "336-456-0878",
                "202-555-0106",
                "MichaelSBiggs@teleworm.us",
                "4792 Keyser Ridge Road",
                "San Francisco"
        );

        this.people = new ArrayList<Person>();
        this.people.add(person1);
        this.people.add(person2);
        this.people.add(person3);
    }

    public ArrayList<Person> getPeople() {
        return this.people;
    }

    public Person getPerson(int id) {

        for (Person person : this.people) {
            if (person.id() == id) {
                return person;
            }
        }

        return null;
    }

    public Person createPerson(Person person) {
        int id = this.getNextId();
        person.setId(id);
        this.people.add(person);
        return person;
    }

    public Person updatePerson(int id, Person person) {

        for (Person p : this.people) {
            if (p.id() == id) {
                Person updatedPerson = new Person(
                        p.id(),
                        person.name() != null ? person.name() : p.name(),
                        person.surname() != null ? person.surname() : p.surname(),
                        person.telephone() != null ? person.telephone() : p.telephone(),
                        person.cellphone() != null ? person.cellphone() : p.cellphone(),
                        person.mail() != null ? person.mail() : p.mail(),
                        person.adress() != null ? person.adress() : p.adress(),
                        person.city() != null ? person.city() : p.city()
                );
                this.people.remove(p);
                this.people.add(updatedPerson);
                return updatedPerson;
            }
        }
        return null;
    }

    public void deletePerson(int id) {

        for (Person p : this.people) {
            if (p.id() == id) {
                this.people.remove(p);
            }
        }
    }

    public ArrayList<Person> searchPeople(String name, String surname) {
        ArrayList<Person> people = new ArrayList<Person>();
        if (name == null && surname == null) {
            return this.getPeople();
        }
        for (Person person : this.people) {
            if (name != null && surname != null && person.name().equals(name) && person.surname().equals(surname)) {
                people.add(person);
            }
            if (name != null && surname == null && person.name().equals(name)) {
                people.add(person);
            }
            if (name == null && surname != null && person.surname().equals(surname)) {
                people.add(person);
            }
        }
        return people;
    }

    public ArrayList<Person> searchPeopleByLocation(String location) {
        ArrayList<Person> people = new ArrayList<Person>();
        for (Person person : this.people) {
            if (person.adress().equals(location) || person.city().equals(location)) {
                people.add(person);
            }
        }
        return people;
    }

    public int getNextId() {
        int max = 0;
        for (Person p : this.people) {
            if (max < p.id()) {
                max = p.id();
            }
        }
        return max + 1;
    }
}