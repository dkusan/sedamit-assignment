package com.sedamit.assignment;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class Person {

    private int id;
    private final String name;
    private final String surname;
    private final String telephone;
    private final String cellphone;
    private final String mail;
    private final String adress;
    private final String city;

    public Person(
            int id,
            String name,
            String surname,
            String telephone,
            String cellphone,
            String mail,
            String adress,
            String city) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.telephone = telephone;
        this.cellphone = cellphone;
        this.mail = mail;
        this.adress = adress;
        this.city = city;
    }

    public void setId(int id){
        this.id = id;
    }

    public int id(){
        return this.id;
    }

    public String name(){
        return this.name;
    }

    public String surname(){
        return this.surname;
    }

    public String telephone(){
        return this.telephone;
    }

    public String cellphone(){
        return this.cellphone;
    }

    public String mail(){
        return this.mail;
    }

    public String adress(){
        return this.adress;
    }

    public String city() { return this.city; }
}